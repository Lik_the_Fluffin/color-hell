# Color hell
Project to make universal color styles (for now only pink)
I don't own most of that code!

## supported sites
- www.youtube.com - looks good
- music.youtube.com - ultrabad
- new wikipedia.org - need some work
- old wikipedia.org - need sowe work


## 📝 Requirements
To install the userstyle in a browser you only need an extension for your browser:
- [Stylish](https://userstyles.org/help/stylish)
- a fork from Stylish (such as: [Stylus](https://github.com/openstyles/stylus))
- or any another extension that embeds user-defined CSS files in websites

## ⚙ Installation 
You can install the userstyle [manually with stylus](#-manual-stylus) or [manually with stylish](#-manual-stylish).
I've lost my Stylish account email! Please use manual install only!

#### 🔧 manual stylus
1. Open one of theme directories (light, dark etc.)
2. Open file with .user.css ending
3. Click on `Open raw`
2. Click on `Install style`

#### 🔧 manual stylish
1. Open one of theme directories (light, dark etc.)
2. Open file with .user.css ending
3. Click on `Copy file contents`
4. Click the `icon` on the toolbar of your browser that is the Stylish extension or a fork such as Stylus (click on the `three dots`) and click on `Manage`.
5. On the left side click on `Create New Style`.
6. On the left side click on `Import`, paste your copied Code from the file and click on `Add to Style`.
7. On the left side enter a name for your userstyle and click on `Save`.

## 👏 Contribution
If you found any bugs, ideas or suggestions for this userstyle, please let me know in the [Issues](https://gitlab.com/Lik_the_Fluffin/color-hell/issues)-Tab. I am happy about new suggestions!
I've made a [guide](https://gitlab.com/Lik_the_Fluffin/color-hell/-/blob/main/GUIDE.md?ref_type=heads)!