#!/bin/bash
 
# Find work directory
wd=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

#
eval "styles_list=($(ls -1d $wd/*/ | sed 's/\///g' | sed "s/$(tr -d '/' <<< $wd)//g" | tr '\n' ' '))"
styles_num=${#styles_list[@]}

#
i=0
while [ $i -lt $styles_num ]
do
cur=${styles_list[$i]}
cat $wd/$cur/$cur.scm > $wd/$cur/$cur.user.css
cat $wd/pre.less >> $wd/$cur/$cur.user.css
lessc $wd/$cur/$cur.user.css $wd/$cur/$cur.user.css
((i++))
done