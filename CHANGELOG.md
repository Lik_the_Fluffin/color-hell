# Last is current in work
# changelog:

### --- v1.5.0  - 2024-1-30 ---

> - New theme! - "dark green" by owokitty
> - Every supported site can have independent color theme
> - Updated www.youtube.com support
> - renamed some themes

### --- v1.4.1  - 2023-12-23 ---

> - small fixes in legacy wikipedia.org

### --- v1.4.0  - 2023-12-23 ---

> - finaly made normal generate script
> - added list of supported sites
> - removed userstyles.org installation, because I lost my account email and cannot more update it 
> - updated author and name info
> - renamed varibles
> - removed "legacy code.css"

### --- Announcement - 2023-12-23 ---

I was stupid and recreated repo, so some changes are lost

---

### --- v1.3.2 - 2023-09-27 ---

> - renamed project to "Color hell"
> - moved the project to a GitLab,
> - fixed youtube like-dislike (changed svg from repo to data url) <br>
> - added GPL3 licence
> - adeed changelog with normal markdown formatting
> - remade readme with normal markdown formatting <br>
https://github.com/FulytheFox/paw-up-userstyle

---

### --- v1.3.1 - 2023-09-20 ---

> - first version of a dark theme <br>

---

### --- v1.3.0 - 2023-09-20 ---

> - using a less preprocessor
> - added "gen.sh" bash script to automize generation of multiple themes <br>

---


### --- v1.2.0 - 2023-09-18 ---

> - chaned a text color for youtube
> - fixed bugs
> - dropped twitter.com support 
> - wikipedia main page support <br>

---

### --- v1.1.1 - 2023-09-17 ---

> - youtube progeess bar
> - youtube like-dislike <br>
https://userstyles.org/styles/238952/pink-heart-progeess-bar-3
https://github.com/FulytheFox/paw-up-userstyle/tree/main
---

### --- v1.1.0 - 2023-09-16 ---

> - wikipedia support  
> - repaired youtube searchbar <br>
https://userstyles.org/styles/155798/wikipedia-star-wars <br>
https://userstyles.org/styles/138552/simple-dark-wikipedia-theme

---


### --- v1.0.0 - 2023-09-16 ---

> - initial release  <br>
https://userstyles.org/styles/167470/kiratto-prichan
